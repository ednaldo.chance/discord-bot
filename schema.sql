CREATE TABLE guild
(
    id BIGINT PRIMARY KEY,
    name TEXT NOT NULL
);


CREATE TABLE guild_config
(
    guild_id BIGINT UNIQUE NOT NULL REFERENCES guild(id) ON DELETE CASCADE,
    timezone TEXT NOT NULL
);


CREATE TABLE discord_user
(
    id BIGINT PRIMARY KEY
);


CREATE TABLE discord_member
(
    id BIGINT NOT NULL,
    guild_id BIGINT NOT NULL REFERENCES guild(id) ON DELETE CASCADE,
    name TEXT NOT NULL,
    PRIMARY KEY(id, guild_id)
);


CREATE TABLE discord_role
(
    id BIGINT PRIMARY KEY,
    guild_id BIGINT NOT NULL REFERENCES guild(id) ON DELETE CASCADE,
    name TEXT NOT NULL,
    is_default BOOLEAN NOT NULL
);

CREATE UNIQUE INDEX ON discord_role(guild_id, is_default) WHERE is_default = TRUE;


CREATE TABLE channel
(
    id BIGINT PRIMARY KEY,
    guild_id  BIGINT NOT NULL REFERENCES guild(id) ON DELETE CASCADE,
    name TEXT NOT NULL
);


CREATE TABLE member_role
(
    member_id BIGINT,
    guild_id BIGINT NOT NULL,
    role_id BIGINT REFERENCES discord_role(id) ON DELETE CASCADE,
    PRIMARY KEY(member_id, role_id),
    FOREIGN KEY(member_id, guild_id) REFERENCES discord_member(id, guild_id) ON DELETE CASCADE
);


CREATE TABLE rolegroup
(
    id SERIAL UNIQUE NOT NULL,
    name TEXT,
    guild_id BIGINT REFERENCES guild(id) ON DELETE CASCADE,
    PRIMARY KEY(name, guild_id)
);


CREATE TABLE rolegroup_role
(
    rolegroup_name TEXT,
    guild_id BIGINT,
    role_id BIGINT REFERENCES discord_role(id) ON DELETE CASCADE,
    PRIMARY KEY(rolegroup_name, guild_id, role_id),
    FOREIGN KEY(rolegroup_name, guild_id) REFERENCES rolegroup(name, guild_id) ON DELETE CASCADE
);


CREATE TABLE ranking
(
    id SERIAL UNIQUE NOT NULL,
    name TEXT,
    guild_id BIGINT REFERENCES guild(id) ON DELETE CASCADE,
    is_default BOOLEAN NOT NULL,
    PRIMARY KEY(name, guild_id)
);


CREATE TABLE ranking_role
(
    ranking_name TEXT,
    guild_id BIGINT,
    role_id BIGINT REFERENCES discord_role(id) ON DELETE CASCADE,
    include BOOLEAN NOT NULL,
    PRIMARY KEY(ranking_name, guild_id, role_id),
    FOREIGN KEY(ranking_name, guild_id) REFERENCES ranking(name, guild_id) ON DELETE CASCADE
);


CREATE TABLE message
(
    id BIGINT PRIMARY KEY,
    user_id BIGINT NOT NULL,
    guild_id BIGINT NOT NULL,
    channel_id BIGINT NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    edited_at TIMESTAMP
);


CREATE TABLE vcsession
(
    id BIGINT PRIMARY KEY,
    user_id BIGINT NOT NULL,
    guild_id BIGINT NOT NULL,
    channel_id BIGINT NOT NULL,
    duration INTEGER NOT NULL,
    began_at TIMESTAMP NOT NULL,
    last_active TIMESTAMP NOT NULL
);


CREATE OR REPLACE FUNCTION insert_default_ranking()
    RETURNS TRIGGER AS
$BODY$
BEGIN
    IF NEW.is_default THEN
        INSERT INTO ranking(name, guild_id, is_default) VALUES('everyone', NEW.guild_id, TRUE);
        INSERT INTO ranking_role VALUES('everyone', NEW.guild_id, NEW.id, TRUE);
    END IF;

    RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE TRIGGER on_role_insert
    AFTER INSERT
    ON discord_role
    FOR EACH ROW
    EXECUTE PROCEDURE insert_default_ranking();


CREATE OR REPLACE FUNCTION insert_guild_config()
    RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO guild_config VALUES(NEW.id, 'UTC');
    RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE TRIGGER on_guild_insert
    AFTER INSERT
    ON guild
    FOR EACH ROW
    EXECUTE PROCEDURE insert_guild_config();


CREATE OR REPLACE FUNCTION get_user_activity(
        _user_id    BIGINT    = NULL,
        _guild_id   BIGINT    = NULL,
        _channel_id BIGINT    = NULL,
        _ts_from    TIMESTAMP = NULL,
        _ts_to      TIMESTAMP = NULL
    )
    RETURNS TABLE (
        user_id       BIGINT,
        message_count INTEGER,
        voice_time    INTEGER,
        active_days   INTEGER,
        last_active   TIMESTAMP
    )
AS $$
BEGIN
    RETURN QUERY
    SELECT
        discord_user.id,
        COALESCE(A.message_count, 0),
        COALESCE(B.voice_time, 0),
        COALESCE(C.active_days, 0),
        C.last_active
    FROM discord_user LEFT JOIN (
        SELECT
            message.user_id,
            CAST(COUNT(*) AS INTEGER) AS message_count
        FROM message WHERE
            ($1 IS NULL OR message.user_id     = $1) AND
            ($2 IS NULL OR message.guild_id    = $2) AND
            ($3 IS NULL OR message.channel_id  = $3) AND
            ($4 IS NULL OR message.created_at >= $4) AND
            ($5 IS NULL OR message.created_at  < $5)
        GROUP BY message.user_id
    ) AS A ON A.user_id = discord_user.id LEFT JOIN (
        SELECT
            vcsession.user_id,
            CAST(SUM(vcsession.duration) AS INTEGER) AS voice_time
        FROM vcsession WHERE
            ($1 IS NULL OR vcsession.user_id      = $1) AND
            ($2 IS NULL OR vcsession.guild_id     = $2) AND
            ($3 IS NULL OR vcsession.channel_id   = $3) AND
            ($4 IS NULL OR vcsession.last_active >= $4) AND
            ($5 IS NULL OR vcsession.last_active  < $5)
        GROUP BY vcsession.user_id
    ) AS B ON B.user_id = discord_user.id LEFT JOIN (
        SELECT
            C3.user_id,
            CAST(COUNT(*) AS INTEGER) AS active_days,
            MAX(C3.last_active) AS last_active
        FROM (
            SELECT
                C2.user_id,
                MAX(C2.last_active) AS last_active,
                C2.day_offset
            FROM (
                SELECT * FROM (
                    SELECT
                        message.user_id,
                        MAX(message.created_at) AS last_active,
                        (CAST(EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) AS INTEGER) - CAST(EXTRACT(EPOCH FROM message.created_at) AS INTEGER)) / 86400 as day_offset
                    FROM message
                    WHERE
                        ($1 IS NULL OR message.user_id     = $1) AND
                        ($2 IS NULL OR message.guild_id    = $2) AND
                        ($3 IS NULL OR message.channel_id  = $3) AND
                        ($4 IS NULL OR message.created_at >= $4) AND
                        ($5 IS NULL OR message.created_at  < $5)
                    GROUP BY message.user_id, day_offset
                ) AS C1 UNION (
                    SELECT
                        vcsession.user_id,
                        MAX(vcsession.last_active) AS last_active,
                        (CAST(EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) AS INTEGER) - CAST(EXTRACT(EPOCH FROM vcsession.last_active) AS INTEGER)) / 86400 as day_offset
                    FROM vcsession
                    WHERE
                        ($1 IS NULL OR vcsession.user_id      = $1) AND
                        ($2 IS NULL OR vcsession.guild_id     = $2) AND
                        ($3 IS NULL OR vcsession.channel_id   = $3) AND
                        ($4 IS NULL OR vcsession.last_active >= $4) AND
                        ($5 IS NULL OR vcsession.last_active  < $5)
                    GROUP BY vcsession.user_id, day_offset
                )
            ) AS C2 GROUP BY C2.user_id, C2.day_offset
        ) AS C3 GROUP BY C3.user_id
    ) AS C ON C.user_id = discord_user.id
    WHERE C.active_days > 0;
END; $$
LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION get_user_scores(
        _mc_weight    REAL      = 0.1,
        _vt_weight    REAL      = 0.016,
        _ad_weight    REAL      = 0.01,
        _guild_id     BIGINT    = NULL,
        _channel_id   BIGINT    = NULL,
        _ranking_name TEXT      = NULL,
        _ts_from      TIMESTAMP = NULL,
        _ts_to        TIMESTAMP = NULL
    )
    RETURNS TABLE (
        user_id BIGINT,
        score   INTEGER
    )
AS $$
BEGIN
    RETURN QUERY
    SELECT
        C.user_id,
        C.score
    FROM (
        SELECT
            B.user_id,
            CAST(FLOOR((B.score + (B.score * ((B.active_days - 1) * $3)))) AS INTEGER) AS score
        FROM (
            SELECT
                A.user_id,
                (A.message_count * $1) + (A.voice_time * $2) AS score,
                A.active_days
            FROM get_user_activity(
                _guild_id   := $4,
                _channel_id := $5,
                _ts_from    := $7,
                _ts_to      := $8
            ) AS A
            WHERE
            (
                A.user_id IN (
                    SELECT
                        discord_member.id
                    FROM discord_member
                    WHERE
                    (
                        ($4 IS NULL OR discord_member.guild_id = $4) AND
                        discord_member.id IN (
                            SELECT
                                member_id
                            FROM member_role
                            WHERE (
                                ($4 IS NULL OR member_role.guild_id = $4) AND
                                member_role.role_id IN (
                                    SELECT
                                        ranking_role.role_id
                                    FROM ranking_role
                                    WHERE
                                        ($6 IS NULL OR ranking_role.ranking_name = $6) AND
                                        ranking_role."include" = true
                                )
                            ) GROUP BY member_role.member_id, member_role.guild_id
                            HAVING COUNT(*) >= 1
                        ) AND discord_member.id NOT IN (
                            SELECT
                                member_role.member_id
                            FROM member_role
                            WHERE (
                                ($4 IS NULL OR guild_id = $4) AND
                                member_role.role_id IN (
                                    SELECT
                                        ranking_role.role_id
                                    FROM ranking_role
                                    WHERE
                                        ($6 IS NULL OR ranking_role.ranking_name = $6) AND
                                        ranking_role."include" = false
                                )
                            ) GROUP BY member_role.member_id, member_role.guild_id
                            HAVING COUNT(*) >= 1
                        )
                    )
                )
            )
        ) AS B
    ) AS C WHERE C.score > 0 ORDER BY C.score;
END; $$
LANGUAGE PLPGSQL;
