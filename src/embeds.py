from datetime import timezone
import discord


class SuccessEmbed(discord.Embed):

    def __init__(self, description):
        green = discord.Colour.from_rgb(0, 255, 0)
        super().__init__(description=description, colour=green)


class FailureEmbed(discord.Embed):

    def __init__(self, description):
        red = discord.Colour.from_rgb(255, 0, 0)
        super().__init__(description=description, colour=red)


class RankingEmbed(discord.Embed):

    @staticmethod
    def _make_ranking(data, pos_width, score_width):
        description = ''
        for is_target, pos, mention, score in data:
            mark = '\u25A3' if is_target else '\u25A2'
            spec = "`{} {pos:0{pos_width}}. {score:0{score_width}}` {mention}\n"
            description += spec.format(
                mark, pos=pos, pos_width=pos_width,
                score=score, score_width=score_width,
                mention=mention)
        return description

    def __init__(self, timeperiod, ranking, target_index, data):
        max_num_of_top_rows = 10
        top_rows = data[:max_num_of_top_rows]

        bottom_rows = []
        if target_index > max_num_of_top_rows - 1:
            max_num_of_bottom_rows = 5
            num_of_bottom_rows = min(len(data) - max_num_of_top_rows, max_num_of_bottom_rows)
            if (len(data) - target_index) < (num_of_bottom_rows // 2):
                i2 = len(data) - 1
                i1 = i2 - num_of_bottom_rows
            else:
                i1 = max(target_index - 2, max_num_of_top_rows)
                i2 = i1 + max_num_of_bottom_rows
            bottom_rows = data[i1:i2]

        max_pos = bottom_rows[-1][1] if bottom_rows else top_rows[-1][1]
        pos_width = len(str(max_pos))

        max_score = top_rows[0][-1]
        score_width = len(str(max_score))

        text = RankingEmbed._make_ranking(top_rows, pos_width, score_width)
        super().__init__(title="Ranking", description=text)

        if bottom_rows:
            text = RankingEmbed._make_ranking(bottom_rows, pos_width, score_width)
            self.add_field(name='Your position', value=text, inline=False)

        footer = "{} ⯈ {}".format(timeperiod, ranking.name)
        self.set_footer(text=footer)


class UserInfoEmbed(discord.Embed):

    def __init__(
            self, username, mention, is_member,
            last_active, joined_at, created_at,
            avatar_url, tz, message_count,
            voice_time, active_days):
        fmt = "%b %d, %Y, %H:%M:%S"
        created_at = created_at.strftime(fmt)

        if last_active is not None:
            last_active = last_active.replace(tzinfo=timezone.utc).astimezone(tz)
            last_active = last_active.strftime(fmt)
        else:
            last_active = "---"

        if joined_at is not None:
            joined_at = joined_at.replace(tzinfo=timezone.utc).astimezone(tz)
            joined_at = joined_at.strftime(fmt)
        else:
            joined_at = "---"

        is_member = {True: "Yes", False: "No"}[is_member]

        super().__init__(title="User information")

        self.add_field(name="Username",       value=username)
        self.add_field(name="Mention",        value=mention)
        self.add_field(name="Member?",         value=is_member)
        self.add_field(name="Last active at", value=last_active)
        self.add_field(name="Joined at",      value=joined_at)
        self.add_field(name="Created at",     value=created_at)
        self.add_field(name="Message count",  value=message_count)
        self.add_field(name="Voice time",     value=seconds_to_string(voice_time))
        self.add_field(name="Active days",    value=active_days)
        self.set_thumbnail(url=avatar_url)


def seconds_to_string(seconds):
    if seconds == 0: return '0'

    days = seconds // 86400
    seconds %= 86400
    hours = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60

    tokens = []
    if days:
        tokens.append(f'{days}d')
    if hours:
        tokens.append(f'{hours}h')
    if minutes:
        tokens.append(f'{minutes}m')
    if seconds:
        tokens.append(f'{seconds}s')

    if len(tokens) == 1:
        return tokens[0]
    else:
        return ', '.join(tokens[:-1]) + ' and ' + tokens[-1]

