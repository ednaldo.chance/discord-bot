import inspect

import discord
import discord.ext.commands as commands


class UserConverter(commands.Converter):

    async def convert(self, ctx, argument):
        bot = ctx.cog.bot
        try:
            return await commands.UserConverter().convert(ctx, argument)
        except:
            pass
        try:
            return await bot.fetch_user(argument)
        except (discord.errors.NotFound, discord.HTTPException):
            pass
        raise commands.errors.BadArgument(f"User **{argument}** not found.")


class MemberConverter(commands.MemberConverter):

    async def convert(self, ctx, argument):
        try:
            return await super().convert(ctx, argument)
        except commands.errors.BadArgument:
            raise commands.errors.BadArgument(f"Member **{argument}** not found.")


class RoleConverter(commands.RoleConverter):

    async def convert(self, ctx, argument):
        try:
            return await super().convert(ctx, argument)
        except commands.errors.BadArgument:
            raise commands.errors.BadArgument(f"Role **{argument}** not found.")


class RolegroupConverter(commands.Converter):

    async def convert(self, ctx, argument):
        make_session = ctx.cog.make_session
        guild = ctx.message.guild
        async with await make_session() as session:
            rolegroup = await session.get_rolegroup_by_name_and_guild(argument, guild)
            if not rolegroup:
                raise commands.errors.BadArgument(f"Rolegroup **{argument}** not found.")
        return rolegroup


class RankingConverter(commands.Converter):

    async def convert(self, ctx, argument):
        make_session = ctx.cog.make_session
        guild = ctx.message.guild
        async with await make_session() as session:
            ranking = await session.get_ranking_by_name_and_guild(argument, guild)
            if not ranking:
                raise commands.errors.BadArgument(f"Ranking **{argument}** not found.")
        return ranking


class TimePeriodValidator(commands.Converter):

    async def convert(self, ctx, argument):
        if argument in ['today', 'lastweek', 'lastmonth', 'alltime']:
            return argument
        else:
            raise commands.errors.BadArgument(f"Time period **{argument}** is invalid.")


def many(converter):
    module = getattr(converter, '__module__', None)
    if module is not None and module.startswith('discord.') and not module.endswith('converter'):
        converter = getattr(commands, converter.__name__ + 'Converter')

    if inspect.isclass(converter):
        if issubclass(converter, commands.Converter):
            instance = converter()
            convert = instance.convert
        else:
            raise TypeError("not a subclass of Converter")
    elif callable(converter):
        async def convert(ctx, arg):
            return converter(arg)
    else:
        raise TypeError("Object is not a converter")

    class Converter(commands.Converter):
        async def convert(self, ctx, argument):
            return await convert(ctx, argument)

    return Converter

