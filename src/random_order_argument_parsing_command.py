import typing
import itertools

import discord
import discord.ext.commands as commands
from discord.ext.commands.converter import _Greedy as Greedy


class RandomOrderArgumentParsingCommand(commands.Command):

    async def _parse_arguments(self, ctx):
        ctx.args = [ctx] if self.cog is None else [self.cog, ctx]

        parameters = iter(self.params.values())
        if self.cog is not None:
            try:
                next(parameters)
            except StopIteration:
                fmt = 'Callback for {0.name} command is missing "self" parameter.'
                raise discord.ClientException(fmt.format(self))

        try:
            next(parameters)
        except StopIteration:
            fmt = 'Callback for {0.name} command is missing "ctx" parameter.'
            raise discord.ClientException(fmt.format(self))

        parameters = list(parameters)
        for param in parameters:
            if param.kind != param.POSITIONAL_OR_KEYWORD:
                message = f"Parameter \"{param.name}\" of command \"{self}\" is not positional"
                raise discord.ClientException(message)
            if param.annotation is param.empty:
                message = f"Parameter \"{param.name}\" of command \"{self}\" is missing a converter"
                raise discord.ClientException(message)
            if param.default is param.empty:
                message = f"Parameter \"{param.name}\" of command \"{self}\" is missing a default value"
                raise discord.ClientException(message)

            origin = getattr(param.annotation, '__origin__', None)
            if isinstance(param.annotation, Greedy) or (origin is not None and (origin is typing.Union or origin is typing.Optional)):
                message = f"Parameter \"{param.name}\" of command \"{self}\" is annotated with an invalid converter"
                raise discord.ClientException(message)

        arguments = []
        iterator = iter(parameters)
        while not ctx.view.eof and next(iterator, None):
            ctx.view.skip_ws()
            arguments.append(ctx.view.get_quoted_word())

        args = await self._actual_parsing(ctx, parameters, arguments)
        ctx.args.extend(args)

        if not self.ignore_extra:
            if not view.eof:
                raise commands.errors.TooManyArguments(
                    'Too many arguments passed to ' + self.qualified_name)

    async def _actual_parsing(self, ctx, parameters, arguments):
        converters = [self._get_converter(param) for param in parameters]
        args =       [param.default        for param in parameters]

        xn = len(arguments)
        yn = len(parameters)
        errors = Matrix(*[[None]  * yn for i in range(xn)])
        ignore = Matrix(*[[False] * yn for i in range(xn)])

        for x, y in itertools.product(range(xn), range(yn)):
            if ignore[x][y]:
                continue
            argument, converter, param = arguments[x], converters[y], parameters[y]
            try:
                args[y] = await ctx.command._actual_conversion(ctx, converter, argument, param)
            except commands.errors.BadArgument as e:
                errors[x][y] = e
            else:
                ignore.set_row_and_col(x, y, True)
                errors.set_row_and_col(x, y, None)

        err = next((e for e in errors.flatten() if e), None)
        if err:
            raise err from None

        return args


class Matrix(object):

    def __init__(self, *rows):
        self.values = rows

    def __getitem__(self, row):
        return self.values[row]

    def flatten(self):
        return list(itertools.chain(*self.values))

    def set_row(self, row, value):
        for j in range(len(self.values[0])):
            self.values[row][j] = value

    def set_col(self, col, value):
        for i in range(len(self.values)):
            self.values[i][col] = value

    def set_row_and_col(self, row, col, value):
        self.set_row(row, value)
        self.set_col(col, value)

