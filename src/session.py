from types import SimpleNamespace
from functools import partial

import persist
from db_models import *


class Session(object):

    def __init__(self, conn):
        self.conn = conn
        self.transaction = self.conn.transaction()

    def __getattr__(self, attr):
        try:
            value = object.__getattr__(self, attr)
        except AttributeError:
            pass
        else:
            return value

        func = getattr(persist, attr, None)
        if func is not None and callable(func):
            return partial(func, self.conn)
        raise AttributeError

    async def __aenter__(self):
        await self.transaction.start()
        return self

    async def __aexit__(self, exc_type, value, traceback):
        if exc_type is not None:
            await self.transaction.rollback()
        else:
            await self.transaction.commit()
        await self.conn.close()

    async def sync(self, bot):
        guilds_in_db = await self.get_guilds()
        guilds_in_discord = bot.guilds
        ids = [g.id for g in guilds_in_discord]
        guilds_to_delete = [g for g in guilds_in_db if g.id not in ids]
        await self.delete_guilds(*guilds_to_delete)
        await self.upsert_guilds(*guilds_in_discord)

        members_in_db = await self.get_members()
        members_in_discord = list(bot.get_all_members())
        ids = [m.id for m in members_in_discord]
        members_to_delete = [to_discord_member(m) for m in members_in_db if m.id not in ids]
        await self.delete_members(*members_to_delete)
        await self.upsert_members(*members_in_discord)

        await self.insert_users(*members_in_discord)

        channels_in_db = await self.get_channels()
        channels_in_discord = list(bot.get_all_channels())
        ids = [c.id for c in channels_in_discord]
        channels_to_delete = [c for c in channels_in_db if c.id not in ids]
        await self.delete_channels(*channels_to_delete)
        await self.upsert_channels(*channels_in_discord)

        roles_in_db = await self.get_roles()
        roles_in_discord = list(bot.get_all_roles())
        ids = [r.id for r in roles_in_discord]
        roles_to_delete = [r for r in roles_in_db if r.id not in ids]
        await self.delete_roles(*roles_to_delete)
        await self.upsert_roles(*roles_in_discord)

        member_roles_in_db = await self.get_member_roles()
        member_roles_in_discord = list(bot.get_member_roles())
        ids = [(m.id, r.id) for m, r in member_roles_in_discord]
        member_roles_to_delete = [
            to_discord_member_and_role(mr)
            for mr in member_roles_in_db if (mr.member_id, mr.role_id) not in ids]
        await self.delete_member_roles(*member_roles_to_delete)
        await self.insert_member_roles(*member_roles_in_discord)


def to_discord_member(member):
    discord_member = SimpleNamespace()
    discord_member.id = member.id
    discord_member.guild = SimpleNamespace()
    discord_member.guild.id = member.guild_id
    discord_member.name = member.name
    return discord_member


def to_discord_member_and_role(member_role):
    member = SimpleNamespace()
    member.id = member_role.member_id
    member.guild = SimpleNamespace()
    member.guild.id = member_role.guild_id
    role = SimpleNamespace()
    role.id = member_role.role_id
    return (member, role)

