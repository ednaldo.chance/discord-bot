import logging
from datetime import datetime, timezone, timedelta

import pytz
import discord
from discord.ext import commands

import errors
from random_order_argument_parsing_command import RandomOrderArgumentParsingCommand
from db_models import Rolegroup, Ranking, RankingRole, GuildConfig
from do_prompt import do_create_rolegroup_prompt, do_create_ranking_prompt
from converters import *
from embeds import *


class MainCog(commands.Cog):
    '''
        Cog containing all the command handlers
    '''
    def __init__(self, bot, make_session):
        self.bot = bot
        self.make_session = make_session

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def rolegroup_create(self, ctx):
        await self.bot.prompt.cancel()
        async with await self.make_session() as session:
            result = await do_create_rolegroup_prompt(ctx, session, self.bot.prompt)
            if result is None:
                return

            name, roles = result
            rolegroup = Rolegroup(name, ctx.message.guild.id)
            await session.insert_rolegroup(rolegroup)
            await session.insert_rolegroup_roles(rolegroup, *roles)

        description = "Rolegroup **{}** was created.".format(name)
        embed = SuccessEmbed(description=description)
        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def rolegroup_delete(self, ctx, rolegroup: RolegroupConverter):
        async with await self.make_session() as session:
            await session.delete_rolegroup(rolegroup)

        description = "Group **{}** was deleted".format(rolegroup.name)
        embed = SuccessEmbed(description=description)
        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def rolegroup_info(self, ctx, rolegroup: RolegroupConverter):
        async with await self.make_session() as session:
            roles = await session.get_rolegroup_roles(rolegroup)

        embed = discord.Embed(title="Rolegroup information")
        embed.add_field(name="Name", value=rolegroup.name, inline=False)
        roles_field_value = ", ".join(role.name for role in roles)
        embed.add_field(name="Roles", value=roles_field_value, inline=False)

        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def rolegroup_list(self, ctx):
        async with await self.make_session() as session:
            rolegroups = await session.get_rolegroups()

        if rolegroups:
            title = f'Rolegroup list ({len(rolegroups)})'
            description = '\n'.join(rg.name for rg in rolegroups)
        else:
            title = 'Rolegroup list'
            description = "No rolegroups have been created"
        embed = discord.Embed(title=title, description=description)

        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command()
    @commands.bot_has_permissions(send_messages=True)
    async def join(self, ctx, role: RoleConverter):
        member = ctx.message.author
        guild = ctx.message.guild

        if role in member.roles:
            raise errors.RejoinRoleError(role.name)

        async with await self.make_session() as session:
            rolegroups = await session.get_conflicting_rolegroups(role, *member.roles)

            roles_to_delete = []
            for rolegroup in rolegroups:
                rolegroup_roles = await session.get_rolegroup_roles(rolegroup)
                rolegroup_roles_ids = [r.id for r in rolegroup_roles]
                conflicting_roles = [r for r in member.roles if r.id in rolegroup_roles_ids]
                roles_to_delete.extend(conflicting_roles)

            member_roles_to_delete = [(member, role) for role in roles_to_delete]
            await session.delete_member_roles(*member_roles_to_delete)
            await session.insert_member_roles((member, role))

        await member.remove_roles(*roles_to_delete)
        await member.add_roles(role)

        description = "You joined **{}**.".format(role.name)
        logging.info(f"User {member.id} joined the role {role.name}")
        embed = SuccessEmbed(description=description)
        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.bot_has_permissions(send_messages=True)
    @commands.command()
    async def leave(self, ctx, role: discord.Role):
        member = ctx.message.author
        if role not in member.roles:
            raise errors.LeaveUnjoinedRoleError(role.name)

        async with await self.make_session() as session:
            await session.delete_member_roles((member, role))

        await member.remove_roles(role)

        description = "You left **{}**.".format(role.name)
        logging.info(f"User {member.id} left the role {role.name}")
        embed = SuccessEmbed(description=description)

        channel = ctx.message.channel
        await channel.send(embed=embed)


    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def ranking_create(self, ctx):
        await self.bot.prompt.cancel()
        async with await self.make_session() as session:
            result = await do_create_ranking_prompt(ctx, session, self.bot.prompt)
            if result is None:
                return
            ranking_name, included, excluded = result
            ranking = Ranking(ranking_name, ctx.guild.id)

            ranking_roles = []
            for role in included:
                rr = RankingRole(ranking.name, ranking.guild_id, role.id, True)
                ranking_roles.append(rr)

            for role in excluded:
                rr = RankingRole(ranking.name, ranking.guild_id, role.id, False)
                ranking_roles.append(rr)

            await session.insert_ranking(ranking)
            await session.insert_ranking_roles(ranking, *ranking_roles)

        description = f"Ranking **{ranking_name}** was created."
        embed = SuccessEmbed(description=description)
        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def ranking_delete(self, ctx, ranking: RankingConverter):
        guild = ctx.message.guild
        async with await self.make_session() as session:
            if ranking.is_default:
                raise errors.CannotDeleteDefaultRanking
            await session.delete_ranking(ranking)

        description = f"Ranking **{ranking.name}** was deleted."
        embed = SuccessEmbed(description=description)
        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def ranking_set_default(self, ctx, ranking: RankingConverter):
        guild = ctx.message.guild
        async with await self.make_session() as session:
            await session.set_default_ranking(ranking, guild)

        description = f"Ranking **{ranking.name}** was set as default."
        embed = SuccessEmbed(description=description)
        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def ranking_info(self, ctx, ranking: RankingConverter):
        async with await self.make_session() as session:
            included, excluded = await session.get_ranking_roles(ranking)

        embed = discord.Embed(title="Ranking information")
        embed.add_field(name="Name", value=ranking.name, inline=False)

        if included:
            roles_field_value = ", ".join(role.name for role in included)
        else:
            roles_field_value = "None"
        embed.add_field(name="Includes", value=roles_field_value, inline=False)

        if excluded:
            roles_field_value = ", ".join(role.name for role in excluded)
        else:
            roles_field_value = "None"
        embed.add_field(name="Excludes", value=roles_field_value, inline=False)

        is_default = "Yes" if ranking.is_default else "No"
        embed.add_field(name="Default", value=is_default, inline=False)

        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def ranking_list(self, ctx):
        guild = ctx.message.guild
        async with await self.make_session() as session:
            rankings = await session.get_rankings(guild)

        def format(ranking):
            formatting = '**' if ranking.is_default else ''
            return "{b}{name}{b}".format(name=ranking.name, b=formatting)

        if rankings:
            title = f"Ranking list ({len(rankings)})"
            description = '\n'.join(format(ranking) for ranking in rankings)
        else:
            title = 'Ranking list'
            description = "No rolegroups have been created"
        embed = discord.Embed(title=title, description=description)

        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command()
    async def user_info(self, ctx, user: UserConverter = None):
        """Returns information about a given user.

        User can be an ID, mention, name#discriminator or a name.
        """
        if user is not None:
            member = discord.utils.get(self.bot.get_all_members(), id=user.id)
        else:
            user = member = ctx.message.author

        guild = ctx.message.guild
        async with await self.make_session() as session:
            guild_config = await session.get_guild_config(guild.id)
            rows = await session.get_user_activity(user=user, guild=guild)

        activity = next(iter(rows), None)
        if activity is not None:
            *activity, last_active = activity[1:]
        if activity is None:
            activity = [0, 0, 0]
            last_active = None

        guild_tz = pytz.timezone(guild_config.timezone)

        username = str(user)
        mention = user.mention
        created_at = user.created_at
        avatar_url = user.avatar_url

        is_member = not not member

        joined_at = None
        if member is not None:
            joined_at = member.joined_at

        embed = UserInfoEmbed(
            username, mention, is_member,
            last_active, joined_at, created_at,
            avatar_url, guild_tz, *activity)
        channel = ctx.message.channel
        await channel.send(embed=embed)

    @commands.command(cls=RandomOrderArgumentParsingCommand)
    async def ranking(
            self,
            ctx,
            member:     MemberConverter     = None,
            timeperiod: TimePeriodValidator = None,
            ranking:    RankingConverter    = None):
        timeperiod = timeperiod if timeperiod is not None else 'lastmonth'
        from_, timeperiod = timeperiod_to_datetime(timeperiod)

        guild = ctx.message.guild
        async with await self.make_session() as session:
            if ranking is None:
                ranking = await session.get_default_ranking(guild)
            scores = await session.get_user_scores(
                guild=guild, from_=from_, ranking=ranking)

        member = ctx.message.author if member is None else member

        data = []
        member_in_ranking = False
        target_index = None
        for index, (user_id, score) in enumerate(scores):
            m = discord.utils.get(guild.members, id=user_id)
            is_target_member = False
            if m.id == member.id:
                member_in_ranking = is_target_member = True
                target_index = index
            position = index + 1
            data.append((is_target_member, position, m.mention, score))

        channel = ctx.message.channel
        if not member_in_ranking:
            description = f"{member.mention} is not in this ranking."
            embed = FailureEmbed(description=description)
            await channel.send(embed=embed)
            return

        embed = RankingEmbed(timeperiod, ranking, target_index, data)
        await channel.send(embed=embed)

    @commands.command()
    @commands.has_permissions(administrator=True)
    @commands.bot_has_permissions(send_messages=True)
    async def set_timezone(self, ctx, timezone):
        try:
            pytz.common_timezones.index(timezone)
        except:
            raise commands.errors.BadArgument("Invalid timezone.")

        guild = ctx.message.guild
        guild_config = GuildConfig(guild.id, timezone)

        async with await self.make_session() as session:
            await session.upsert_guild_configs(guild_config)

        description = f"Timezone set to **{timezone}** for **{guild.name}**."
        embed = SuccessEmbed(description=description)

        channel = ctx.message.channel
        await channel.send(embed=embed)


def timeperiod_to_datetime(timeperiod):
    now = datetime.now()

    lastweek  = now - timedelta(days=7)
    lastmonth = now - timedelta(days=30)
    from_, timeperiod = {
        'today':     (now,       "Today"),
        'lastweek':  (lastweek,  "Last Week"),
        'lastmonth': (lastmonth, "Last Month"),
        'alltime':   (None,      "All Time")
    }[timeperiod]
    return from_, timeperiod

