import asyncio
import logging

from discord.ext import commands, tasks


class DataCollectionCog(commands.Cog):

    UPDATE_PERIOD_IN_SECONDS = 5

    def __init__(self, bot, make_session):
        self.bot = bot
        self.make_session = make_session
        self.lock = asyncio.Lock()
        self.active_members = set()
        self.waiting_list = set()

        self.bot.loop.create_task(self._init_active_members())
        self._update.start()

    def cog_unload(self):
        self._update.cancel()

    async def _init_active_members(self):
        await self.lock.acquire()

        voice_channels = self.bot.get_all_voice_channels()
        members_in_voice = [m for c in voice_channels for m in c.members]
        for member in members_in_voice:
            voice_state = member.voice
            if utils.is_voice_state_active(voice_state):
                self.active_members.add(member)

        self.lock.release()

    @tasks.loop(seconds=UPDATE_PERIOD_IN_SECONDS)
    async def _update(self):
        await self.lock.acquire()

        async with await self.make_session() as session:
            seconds = DataCollectionCog.UPDATE_PERIOD_IN_SECONDS
            await session.upsert_current_vcsessions(seconds, *self.active_members)

        self.active_members.update(self.waiting_list)
        self.waiting_list = set()

        self.lock.release()

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        logging.warning(f"Joining server {guild.id} with name {guild.name}")

        async with await self.make_session() as session:
            await session.insert_guilds(guild)
            await session.insert_members(*guild.members)
            await session.insert_channels(*guild.channels)
            await session.insert_roles(*guild.roles)
            await session.insert_member_roles(*self.bot.get_member_roles(guild))

    @commands.Cog.listener()
    async def on_guild_remove(self, guild):
        logging.warning(f"Leaving server {guild.id} with name {guild.name}")

        async with await self.make_session() as session:
            await session.delete_guilds(guild)

    @commands.Cog.listener()
    async def on_guild_update(self, before, after):
        logging.warning(f"The server {before.id} has been updated")

        async with await self.make_session() as session:
            await session.upsert_guilds(after)

    @commands.Cog.listener()
    async def on_member_join(self, member):
        logging.warning(f"The user {member.id} has joined the server")

        async with await self.make_session() as session:
            await session.insert_users(member)
            await session.insert_members(member)
            await session.insert_member_roles((member, member.guild.default_role))

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        logging.warning(f"The user {member.id} has left")

        async with await self.make_session() as session:
            await session.delete_members(member)

    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        if (before.name != after.name):
            logging.warning(f"The user {before.id} has been updated")
            logging.warning(f"Before: {before.name}, After: {after.name}")

        async with await self.make_session() as session:
            await session.upsert_members(after)

            new_roles = set(after.roles) - set(before.roles)
            member_roles_to_add = [(after, role) for role in new_roles]
            await session.insert_member_roles(*member_roles_to_add)

            old_roles = set(before.roles) - set(after.roles)
            member_roles_to_delete = [(after, role) for role in old_roles]
            await session.delete_member_roles(*member_roles_to_delete)

    @commands.Cog.listener()
    async def on_guild_channel_create(self, channel):
        logging.warning(f"The channel {channel.name} has been created")

        async with await self.make_session() as session:
            await session.insert_channels(channel)

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel):
        logging.warning(f"The channel {channel.name} has been deleted")

        async with await self.make_session() as session:
            await session.delete_channels(channel)

    @commands.Cog.listener()
    async def on_guild_channel_update(self, before, after):
        logging.warning(f"The channel {before.name} has been updated")

        async with await self.make_session() as session:
            await session.upsert_channels(after)

    @commands.Cog.listener()
    async def on_guild_role_create(self, role):
        logging.warning(f"The role {role.name} has been created")

        async with await self.make_session() as session:
            await session.insert_roles(role)

    @commands.Cog.listener()
    async def on_guild_role_delete(self, role):
        logging.warning(f"The role {role.name} has been deleted")

        async with await self.make_session() as session:
            await session.delete_roles(role)

    @commands.Cog.listener()
    async def on_guild_role_update(self, before, after):
        logging.warning(f"The role {before.name} has been updated")

        async with await self.make_session() as session:
            await session.upsert_roles(after)

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.guild is None:
            return

        async with await self.make_session() as session:
            await session.insert_messages(message)

        await self.bot.process_commands(message)

    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
        async with await self.make_session() as session:
            await session.upsert_messages(after)

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        await self.lock.acquire()

        was_active = is_voice_state_active(before)
        is_active  = is_voice_state_active(after)

        if not was_active and is_active:
            self.waiting_list.add(member)
        elif was_active and not is_active:
            if member in self.waiting_list:
                self.waiting_list.remove(member)
            elif member in self.active_members:
                self.active_members.remove(member)

        self.lock.release()


def is_voice_state_active(voice_state):
    is_in_a_channel = voice_state.channel is not None
    return (
        is_in_a_channel and
        not voice_state.mute and
        not voice_state.self_mute and
        not voice_state.deaf and
        not voice_state.self_deaf)

