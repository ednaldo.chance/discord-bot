from dataclasses import dataclass, astuple
from datetime import datetime, date


@dataclass
class DbModel(object):

    def astuple(self):
        return astuple(self)


@dataclass
class Guild(DbModel):

    id: int
    name: str

    @staticmethod
    def from_discord_model(guild):
        return Guild(guild.id, guild.name)

    @staticmethod
    def from_discord_models(guilds):
        return [Guild.from_discord_model(guild) for guild in guilds]

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.id)


@dataclass
class GuildConfig(DbModel):

    guild_id: int
    timezone: str

    def __eq__(self, other):
        return self.guild_id == other.guild_id

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.guild_id)


@dataclass
class User(DbModel):

    id: int

    @staticmethod
    def from_discord_model(user):
        return User(user.id)

    @staticmethod
    def from_discord_models(users):
        return [User.from_discord_model(user) for user in users]

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.id)


@dataclass
class Member(DbModel):

    id: int
    guild_id: int
    name: str

    @staticmethod
    def from_discord_model(member):
        return Member(member.id, member.guild.id, member.name)

    @staticmethod
    def from_discord_models(members):
        return [Member.from_discord_model(member) for member in members]

    def __eq__(self, other):
        return self.id == other.id and self.guild_id == other.guild_id

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self.id, self.guild_id))


@dataclass
class Role(DbModel):

    id: int
    guild_id: int
    name: str
    is_default: bool = False

    @staticmethod
    def from_discord_model(role):
        return Role(role.id, role.guild.id, role.name, role.is_default())

    @staticmethod
    def from_discord_models(roles):
        return [Role.from_discord_model(role) for role in roles]

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.id)


@dataclass
class Channel(DbModel):

    id: int
    guild_id: int
    name: str

    @staticmethod
    def from_discord_model(channel):
        return Channel(channel.id, channel.guild.id, channel.name)

    @staticmethod
    def from_discord_models(channels):
        return [Channel.from_discord_model(channel) for channel in channels]

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.id)


@dataclass
class MemberRole(DbModel):

    member_id: int
    guild_id: int
    role_id: int

    @staticmethod
    def from_discord_model(member, role):
        return MemberRole(member.id, member.guild.id, role.id)

    @staticmethod
    def from_discord_models(member_roles):
        return [MemberRole.from_discord_model(member, role) for member, role in member_roles]

    def __eq__(self, other):
        return (self.member_id == other.member_id and
            self.guild_id == other.guild_id and
            self.role_id == other.role_id)

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self.member_id, self.guild_id, self.role_id))


@dataclass
class Rolegroup(DbModel):

    name: str
    guild_id: int

    def __eq__(self, other):
        return (self.name == other.name and
            self.guild_id == other.guild_id)

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self.name, self.guild_id))


@dataclass
class Ranking(DbModel):

    name: str
    guild_id: int
    is_default: bool = False

    def __eq__(self, other):
        return (self.name == other.name and
            self.guild_id == other.guild_id)

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self.name, self.guild_id))


@dataclass
class RankingRole(DbModel):

    ranking_name: str
    guild_id: int
    role_id: int
    include: bool

    def __eq__(self, other):
        return (self.name == other.name and
                self.guild_id == other.guild_id)

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self.ranking_name, self.guild_id, self.role_id))


@dataclass
class Message(DbModel):

    id: int
    user_id: int
    guild_id: int
    channel_id: int
    content: str
    created_at: datetime
    edited_at: datetime

    @staticmethod
    def from_discord_model(message):
        return Message(
            message.id,
            message.author.id,
            message.guild.id,
            message.channel.id,
            message.content,
            message.created_at,
            message.edited_at)

    @staticmethod
    def from_discord_models(messages):
        return [Message.from_discord_model(message) for message in messages]

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.id)


@dataclass
class VoiceSession(DbModel):

    id: int
    user_id: int
    guild_id: int
    channel_id: int
    duration: int
    began_at: datetime
    last_active: datetime

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.id)


@dataclass
class UserActivity(DbModel):

    user_id: int
    guild_id: int
    active_date: date
    last_active: datetime
    message_count: int
    voice_time: int

    def __eq__(self, other):
        return (self.user_id == other.id and
            self.guild_id == other.guild_id and
            self.active_date == other.active_date)

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self.user_id, self.guild_id, self.active_date))

