import asyncio

import discord


class Prompt(object):

    def __init__(self, bot):
        self.bot = bot
        self._task = None
        self._message = None

    async def cancel(self):
        if self._task is not None:
            self._task.cancel()
            self._task = None

        if self._message is not None:
            try: 
                await self._message.delete()
            except discord.errors.NotFound:
                pass

    async def update(self, ctx, content):
        if self._message is None:
            self._message = await ctx.channel.send(content=content)
            return

        try:
            await self._message.edit(content=content)
        except:
            self._message = await ctx.channel.send(content=content)

    async def wait_for_answer(self, check=None, timeout=None):
        coro = self.bot.wait_for('message', check=check, timeout=timeout)
        self._task = asyncio.create_task(coro)

        try:
            message = await self._task
        except asyncio.TimeoutError:
            return None
        else:
            return message

