import logging

import discord
import discord.ext.commands as commands
import asyncpg

import errors
import g
import secret
from main_cog import MainCog
from data_collection_cog import DataCollectionCog
from session import Session
from prompt import Prompt
from embeds import FailureEmbed


class Bot(commands.Bot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, command_prefix='::', status=discord.Status.dnd, **kwargs)
        self.prompt = Prompt(self)

    async def _prepare(self):
        await self.wait_until_ready()

        self.conn_pool = await asyncpg.create_pool(
            user=secret.PG_USER, password=secret.PG_PASS,
            database=g.dbname)

        async def make_session():
            conn = await self.conn_pool.acquire()
            return Session(conn)

        async with await make_session() as session:
            logging.info("Starting sync")
            await session.sync(self)
            logging.info("Finishing sync")

        self.add_cog(MainCog(self, make_session))
        self.add_cog(DataCollectionCog(self, make_session))

        await self.change_presence(status=discord.Status.online)

    def run(self, token):
        try:
            self.loop.create_task(self._prepare())
            super().run(token)
        except KeyboardInterrupt:
            pass

    async def on_message(self, message):
        return

    async def on_command_error(self, ctx, error):
        error = getattr(error, 'original', error)
        unexpected = False

        if isinstance(error, (errors.BaseError, commands.errors.BadArgument)):
            description = str(error)
        elif isinstance(error, commands.errors.MissingRequiredArgument):
            description = f"Missing **{error.param.name}** parameter."
        elif isinstance(error, commands.errors.CommandNotFound):
            return
        else:
            description = "An unexpected error has ocurred."
            unexpected = True

        embed = FailureEmbed(description=description)
        channel = ctx.message.channel
        await channel.send(embed=embed)

        if unexpected:
            raise error

    def get_all_roles(self):
        return (role for guild in self.guilds for role in guild.roles)

    def get_all_voice_channels(self):
        for channel in self.get_all_channels():
            if isinstance(channel, discord.VoiceChannel):
                yield channel

    def get_member_roles(self, guild=None):
        members = guild.members if guild is not None else self.get_all_members()
        return ((member, role) for member in members for role in member.roles)

    async def get_user_from_string(self, ctx, string):
        try:
            return await commands.UserConverter().convert(ctx, string)
        except commands.BadArgument:
            pass
        try:
            return await self.fetch_user(string)
        except (discord.errors.NotFound, discord.HTTPException):
            pass
        try:
            return await self.fetch_user(int(string[2:-1]))
        except (discord.errors.NotFound, ValueError):
            pass
        return None

