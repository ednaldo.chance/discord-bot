from datetime import datetime, timedelta
from collections import defaultdict

from pypika import PostgreSQLQuery as Query
from pypika import Table, Field, Parameter, Order
from pypika.terms import Term
from pypika import functions as fn

from db_models import *


t_guild          = Table('guild')
t_guild_config   = Table('guild_config')
t_user           = Table('discord_user')
t_member         = Table('discord_member')
t_channel        = Table('channel')
t_role           = Table('discord_role')
t_member_role    = Table('member_role')
t_rolegroup      = Table('rolegroup')
t_rolegroup_role = Table('rolegroup_role')
t_ranking        = Table('ranking')
t_ranking_role   = Table('ranking_role')
t_message        = Table('message')
t_vcsession      = Table('vcsession')


async def get_guilds(conn):
    query = Query.from_(t_guild).select('*')
    rows = await conn.fetch(str(query))
    return [Guild(*row) for row in rows]


async def insert_guilds(conn, *guilds):
    if not guilds: return
    data = [(guild.id, guild.name) for guild in guilds]
    query = Query.into(t_guild).insert(
        Parameter('$1'), Parameter('$2')
    ).do_nothing()
    await conn.executemany(str(query), data)


async def update_guilds(conn, *guilds):
    if not guilds: return
    data = [(guild.id, guild.name) for guild in guilds]
    query = (Query.update(t_guild)
        .set(t_guild.name, Parameter('$2'))
    ).where(t_guild.id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def upsert_guilds(conn, *guilds):
    await insert_guilds(conn, *guilds)
    await update_guilds(conn, *guilds)


async def delete_guilds(conn, *guilds):
    if not guilds: return
    data = [(guild.id,) for guild in guilds]
    query = Query.from_(t_guild).delete().where(
        t_guild.id == Parameter('$1')
    )
    await conn.executemany(str(query), data)


async def get_guild_config(conn, id):
    query = Query.from_(t_guild_config).select('*').where(
        t_guild_config.guild_id == Parameter('$1')
    )
    data = await conn.fetchrow(str(query), id)
    if data is not None:
        return GuildConfig(*data)
    return None


async def insert_guild_configs(conn, *guild_configs):
    if not guild_configs: return
    data = [config.astuple() for config in guild_configs]
    query = Query.into(t_guild_config).insert(
        Parameter('$1'), Parameter('$2')
    ).do_nothing()
    await conn.executemany(str(query), data)


async def update_guild_configs(conn, *guild_configs):
    if not guild_configs: return
    data = [config.astuple() for config in guild_configs]
    query = (Query.update(t_guild_config)
        .set(t_guild_config.timezone, Parameter('$2'))
    ).where(t_guild_config.guild_id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def upsert_guild_configs(conn, *guild_configs):
    await insert_guild_configs(conn, *guild_configs)
    await update_guild_configs(conn, *guild_configs)


async def delete_guild_configs(conn, *guild_configs):
    if not guild_configs: return
    data = [(config.guild_id,) for config in guild_configs]
    query = Query.from_(t_guild_config).delete().where(
        t_guild_config.guild_id == Parameter('$1')
    )
    await conn.executemany(str(query), data)


async def get_users(conn):
    query = Query.from_(t_user).select('*')
    rows = await conn.fetch(str(query))
    return [User(*row) for row in rows]


async def insert_users(conn, *users):
    if not users: return
    data = [(user.id,) for user in users]
    query = Query.into(t_user).insert(Parameter('$1')).do_nothing()
    await conn.executemany(str(query), data)


async def delete_users(conn, *users):
    if not users: return
    data = [(user.id,) for user in users]
    query = Query.from_(t_user).delete().where(t_user.id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def get_members(conn):
    query = Query.from_(t_member).select('*')
    rows = await conn.fetch(str(query))
    return [Member(*row) for row in rows]


async def insert_members(conn, *members):
    if not members: return
    data = [(member.id, member.guild.id, member.name) for member in members]
    query = Query.into(t_member).insert(
        Parameter('$1'), Parameter('$2'), Parameter('$3')
    ).do_nothing()
    await conn.executemany(str(query), data)


async def update_members(conn, *members):
    if not members: return
    data = [(member.id, member.name) for member in members]
    query = Query.update(t_member).set(
        Parameter('name'), Parameter('$2')
    ).where(t_member.id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def upsert_members(conn, *members):
    await insert_members(conn, *members)
    await update_members(conn, *members)


async def delete_members(conn, *members):
    if not members: return
    data = [(member.id, member.guild.id) for member in members]
    query = Query.from_(t_member).delete().where(
        (t_member.id == Parameter('$1')) &
        (t_member.guild_id == Parameter('$2'))
    )
    await conn.executemany(str(query), data)


async def get_channels(conn):
    query = Query.from_(t_channel).select('*')
    rows = await conn.fetch(str(query))
    return [Channel(*row) for row in rows]


async def insert_channels(conn, *channels):
    if not channels: return
    data = [(channel.id, channel.guild.id, channel.name) for channel in channels]
    query = Query.into(t_channel).insert(
        Parameter('$1'), Parameter('$2'), Parameter('$3')
    ).do_nothing()
    await conn.executemany(str(query), data)


async def update_channels(conn, *channels):
    if not channels: return
    data = [(channel.id, channel.name) for channel in channels]
    query = Query.update(t_channel).set(
        t_channel.name, Parameter('$2')
    ).where(t_channel.id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def upsert_channels(conn, *channels):
    await insert_channels(conn, *channels)
    await update_channels(conn, *channels)


async def delete_channels(conn, *channels):
    if not channels: return
    data = [(channel.id,) for channel in channels]
    query = Query.from_(t_channel).delete().where(t_channel.id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def get_roles(conn):
    query = Query.from_(t_role).select('*')
    rows = await conn.fetch(str(query))
    return [Role(*row) for row in rows]


async def insert_roles(conn, *roles):
    if not roles: return
    data = [(role.id, role.guild.id, role.name, role.is_default()) for role in roles]
    query = Query.into(t_role).insert(
        Parameter('$1'),
        Parameter('$2'),
        Parameter('$3'),
        Parameter('$4')
    ).do_nothing()
    await conn.executemany(str(query), data)


async def update_roles(conn, *roles):
    if not roles: return
    data = tuple((role.id, role.name) for role in roles)
    query = Query.update(t_role).set(
        t_role.name, Parameter('$2')
    ).where(t_role.id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def upsert_roles(conn, *roles):
    await insert_roles(conn, *roles)
    await update_roles(conn, *roles)


async def delete_roles(conn, *roles):
    if not roles: return
    data = [(role.id,) for role in roles]
    query = Query.from_(t_role).delete().where(t_role.id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def get_member_roles(conn):
    query = Query.from_(t_member_role).select('*')
    rows = await conn.fetch(str(query))
    return [MemberRole(*row) for row in rows]


async def insert_member_roles(conn, *member_roles):
    if not member_roles: return
    data = [(m.id, m.guild.id, r.id) for (m, r) in member_roles]
    query = Query.into(t_member_role).insert(
        Parameter('$1'), Parameter('$2'), Parameter('$3')
    ).do_nothing()
    await conn.executemany(str(query), data)


async def delete_member_roles(conn, *member_roles):
    if not member_roles: return
    data = [(m.id, r.id) for (m, r) in member_roles]
    query = Query.from_(t_member_role).delete().where(
        (t_member_role.member_id == Parameter('$1')) &
        (t_member_role.role_id == Parameter('$2'))
    )
    await conn.executemany(str(query), data)


async def get_rolegroups(conn):
    query = Query.from_(t_rolegroup).select(
        t_rolegroup.name, t_rolegroup.guild_id
    ).orderby(t_rolegroup.id)
    rows = await conn.fetch(str(query))
    return [Rolegroup(*row) for row in rows]


async def get_rolegroup_by_name_and_guild(conn, name, guild):
    query = Query.from_(t_rolegroup).select(
        t_rolegroup.name, t_rolegroup.guild_id
    ).where(
        (t_rolegroup.name == Parameter('$1')) &
        (t_rolegroup.guild_id == Parameter('$2'))
    )
    row = await conn.fetchrow(str(query), name, guild.id)
    if row is not None:
        return Rolegroup(*row)
    return None


async def get_rolegroup_from_roles(conn, *roles):
    role_ids = [role.id for role in roles]

    query = Query.from_(t_rolegroup_role).select(
        t_rolegroup_role.rolegroup_name
    ).where(t_rolegroup_role.role_id.isin(role_ids)).groupby(
        t_rolegroup_role.rolegroup_name
    ).having(fn.Count('*') >= len(roles))

    query = Query.from_(t_rolegroup).select(
        t_rolegroup.name, t_rolegroup.guild_id
    ).join(t_rolegroup_role).on(
        t_rolegroup_role.rolegroup_name == t_rolegroup.name
    ).where(t_rolegroup_role.rolegroup_name.isin(query)).groupby(
        t_rolegroup.name, t_rolegroup.guild_id
    ).having(fn.Count('*') == len(roles))

    row = await conn.fetchrow(str(query))
    if row is not None:
        return Rolegroup(*row)
    return None


async def get_conflicting_rolegroups(conn, *roles):
    role_ids = [role.id for role in roles]

    query = Query.from_(t_rolegroup).select(
        t_rolegroup.name, t_rolegroup.guild_id
    ).join(t_rolegroup_role).on(
        t_rolegroup_role.rolegroup_name == t_rolegroup.name
    ).where(t_rolegroup_role.role_id.isin(role_ids)).groupby(
        t_rolegroup.name, t_rolegroup.guild_id
    ).having(fn.Count('*') >= 2)

    rows = await conn.fetch(str(query))
    return [Rolegroup(*row) for row in rows]


async def insert_rolegroup(conn, rolegroup):
    query = Query.into(t_rolegroup).columns(
        t_rolegroup.name, t_rolegroup.guild_id
    ).insert(Parameter('$1'), Parameter('$2')).do_nothing()
    await conn.execute(str(query), rolegroup.name, rolegroup.guild_id)


async def delete_rolegroup(conn, rolegroup):
    query = Query.from_(t_rolegroup).delete().where(
        (t_rolegroup.name == Parameter('$1')) &
        (t_rolegroup.guild_id == Parameter('$2'))
    )
    await conn.execute(str(query), rolegroup.name, rolegroup.guild_id)


async def get_rolegroup_roles(conn, rolegroup):
    query = Query.from_(t_role).select(t_role.star).join(t_rolegroup_role).on(
        t_rolegroup_role.role_id == t_role.id
    ).where(
        (t_rolegroup_role.rolegroup_name == Parameter('$1')) &
        (t_rolegroup_role.guild_id == Parameter('$2'))
    )
    rows = await conn.fetch(str(query), rolegroup.name, rolegroup.guild_id)
    return [Role(*row) for row in rows]


async def insert_rolegroup_roles(conn, rolegroup, *roles):
    data = [(rolegroup.name, rolegroup.guild_id, role.id) for role in roles]
    query = Query.into(t_rolegroup_role).columns(
        t_rolegroup_role.rolegroup_name,
        t_rolegroup_role.guild_id,
        t_rolegroup_role.role_id
    ).insert(Parameter('$1'), Parameter('$2'), Parameter('$3'))
    await conn.executemany(str(query), data)


async def get_rankings(conn, guild):
    query = Query.from_(t_ranking).select(
        t_ranking.name, t_ranking.guild_id, t_ranking.is_default
    ).where(t_ranking.guild_id == Parameter('$1')).orderby(t_ranking.id)
    rows = await conn.fetch(str(query), guild.id)
    return [Ranking(*row) for row in rows]


async def get_ranking_by_name_and_guild(conn, name, guild):
    query = Query.from_(t_ranking).select(
        t_ranking.name, t_ranking.guild_id, t_ranking.is_default
    ).where((t_ranking.name == Parameter('$1')) & (t_ranking.guild_id == Parameter('$2')))
    row = await conn.fetchrow(str(query), name, guild.id)
    if row is not None:
        return Ranking(*row)
    return None


async def get_default_ranking(conn, guild):
    query = Query.from_(t_ranking).select(
        t_ranking.name, t_ranking.guild_id, t_ranking.is_default
    ).where((t_ranking.guild_id == Parameter('$1')) & (t_ranking.is_default == True))
    row = await conn.fetchrow(str(query), guild.id)
    if row is not None:
        return Ranking(*row)
    return None


async def insert_ranking(conn, ranking):
    query = Query.into(t_ranking).columns(
        t_ranking.name, t_ranking.guild_id, t_ranking.is_default
    ).insert(Parameter('$1'), Parameter('$2'), Parameter('$3')).do_nothing()
    args = (ranking.name, ranking.guild_id, ranking.is_default)
    await conn.execute(str(query), *args)


async def delete_ranking(conn, ranking):
    query = Query.from_(t_ranking).delete().where(
        (t_ranking.name == Parameter('$1')) & (t_ranking.guild_id == Parameter('$2'))
    )
    await conn.execute(str(query), ranking.name, ranking.guild_id)


async def set_default_ranking(conn, ranking, guild):
    rankings = await get_rankings(conn, guild)
    data = [(r.name, guild.id, False) for r in rankings]
    query = Query.update(t_ranking).set(t_ranking.is_default, Parameter('$3')).where(
        (t_ranking.name == Parameter('$1')) & (t_ranking.guild_id == Parameter('$2'))
    )
    await conn.executemany(str(query), data)
    await conn.execute(str(query), ranking.name, ranking.guild_id, True)


async def get_ranking_roles(conn, ranking):
    query = Query.from_(t_role).select(t_role.star).join(t_ranking_role).on(
        t_ranking_role.role_id == t_role.id
    ).where(
        (t_ranking_role.ranking_name == Parameter('$1')) &
        (t_ranking_role.guild_id == Parameter('$2')) &
        (t_ranking_role.include == True)
    )
    rows = await conn.fetch(str(query), ranking.name, ranking.guild_id)
    included = [Role(*row) for row in rows]

    query = Query.from_(t_role).select(t_role.star).join(t_ranking_role).on(
        t_ranking_role.role_id == t_role.id
    ).where(
        (t_ranking_role.ranking_name == Parameter('$1')) &
        (t_ranking_role.guild_id == Parameter('$2')) &
        (t_ranking_role.include == False)
    )
    rows = await conn.fetch(str(query), ranking.name, ranking.guild_id)
    excluded = [Role(*row) for row in rows]

    return included, excluded


async def insert_ranking_roles(conn, ranking, *ranking_roles):
    data = [(ranking.name, ranking.guild_id, rr.role_id, rr.include) for rr in ranking_roles]
    query = Query.into(t_ranking_role).columns(
        t_ranking_role.ranking_name,
        t_ranking_role.guild_id,
        t_ranking_role.role_id,
        t_ranking_role.include
    ).insert(
        Parameter('$1'),
        Parameter('$2'),
        Parameter('$3'),
        Parameter('$4')
    )
    await conn.executemany(str(query), data)


async def insert_messages(conn, *messages):
    if not messages: return
    data = [(m.id, m.author.id, m.guild.id, m.channel.id,
            m.content, m.created_at, m.edited_at) for m in messages]

    query = Query.into(t_message).insert(
        Parameter('$1'),
        Parameter('$2'),
        Parameter('$3'),
        Parameter('$4'),
        Parameter('$5'),
        Parameter('$6'),
        Parameter('$7')
    ).do_nothing()
    await conn.executemany(str(query), data)


async def update_messages(conn, *messages):
    if not messages: return

    data = [(m.id, m.content, m.edited_at) for m in messages]
    query = (Query.update(t_message)
        .set(t_message.content, Parameter('$2'))
        .set(t_message.edited_at, Parameter('$3'))
    ).where(t_message.id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def upsert_messages(conn, *messages):
    await insert_messages(conn, *messages)
    await update_messages(conn, *messages)


async def get_vcsessions(conn, *users, guild=None, from_=None, to=None):
    args = []

    query = Query.from_(t_vcsession).select('*')
    if guild is not None:
        query = query.where(t_vcsession.guild_id    == _add_arg(args, guild.id))
    if from_ is not None:
        query = query.where(t_vcsession.last_active >= _add_arg(args, from_))
    if to is not None:
        query = query.where(t_vcsession.last_active  < _add_arg(args, to))

    async with UserIdTempTable(conn, users):
        rows = await conn.fetch(str(query), *args)

    return [VoiceSession(*row) for row in rows]


async def get_vcsession_max_id(conn):
    query = (Query.from_(t_vcsession).select('*')
        .orderby(t_vcsession.id, order=Order.desc)
        .limit(1)
    )
    row = await conn.fetchrow(str(query))
    if row is not None:
        return row[0]
    return None


async def insert_vcsessions(conn, *sessions):
    if not sessions: return

    data = [session.astuple() for session in sessions]
    query = Query.into(t_vcsession).insert(
        Parameter('$1'),
        Parameter('$2'),
        Parameter('$3'),
        Parameter('$4'),
        Parameter('$5'),
        Parameter('$6'),
        Parameter('$7')
    ).do_nothing()
    await conn.executemany(str(query), data)


async def update_vcsessions(conn, *sessions):
    if not sessions: return

    data = [(s.id, s.duration, s.last_active) for s in sessions]
    query = (Query.update(t_vcsession)
        .set(t_vcsession.duration, Parameter('$2'))
        .set(t_vcsession.last_active, Parameter('$3'))
    ).where(t_vcsession.id == Parameter('$1'))
    await conn.executemany(str(query), data)


async def upsert_vcsessions(conn, *sessions):
    await insert_vcsessions(conn, *sessions)
    await update_vcsessions(conn, *sessions)


async def upsert_current_vcsessions(conn, seconds, *members):
    if not members: return

    now = datetime.utcnow()
    today = now - timedelta(minutes=5)

    recent_sessions = await get_vcsessions(conn, from_=today, *members)

    user_id_to_sessions = defaultdict(list)
    for session in recent_sessions:
        user_id = session.user_id
        user_id_to_sessions[user_id].append(session)

    max_id = await get_vcsession_max_id(conn)

    sessions_to_upsert = []
    new_session_count = 0
    for member in members:
        session = user_id_to_sessions.get(member.id, None)
        session = session[0] if session is not None else None
        if session is not None:
            session.duration += seconds
            session.last_active = now
        else:
            new_session_count += 1
            session_id = (max_id + new_session_count
                if max_id else new_session_count)
            channel = member.voice.channel
            session = VoiceSession(
                session_id, member.id, member.guild.id,
                channel.id, seconds, now, now)
        sessions_to_upsert.append(session)
    await upsert_vcsessions(conn, *sessions_to_upsert)


async def get_user_activity(conn, user=None, guild=None, channel=None, from_=None, to=None):
    args = []
    kwargs = {}
    if user is not None:
        kwargs['_user_id']    = _add_arg(args, user.id)
    if guild is not None:
        kwargs['_guild_id']   = _add_arg(args, guild.id)
    if channel is not None:
        kwargs['_channel_id'] = _add_arg(args, channel.id)
    if from_ is not None:
        kwargs['_ts_from']    = _add_arg(args, from_)
    if to is not None:
        kwargs['_ts_to']      = _add_arg(args, to)

    argument_list = ', '.join(['{k} := {v}'.format(k=k, v=v) for k,v in kwargs.items()])

    query = f"SELECT * FROM get_user_activity({argument_list})"
    rows = await conn.fetch(str(query), *args)
    return rows


async def get_user_scores(conn, guild=None, channel=None, ranking=None, from_=None, to=None):
    args = []
    kwargs = {}
    if guild is not None:
        kwargs['_guild_id']     = _add_arg(args, guild.id)
    if channel is not None:
        kwargs['_channel_id']   = _add_arg(args, channel.id)
    if ranking is not None:
        kwargs['_ranking_name'] = _add_arg(args, ranking.name)
    if from_ is not None:
        kwargs['_ts_from']      = _add_arg(args, from_)
    if to is not None:
        kwargs['_ts_to']        = _add_arg(args, to)

    argument_list = ', '.join(['{k} := {v}'.format(k=k, v=v) for k,v in kwargs.items()])

    query = f"SELECT * FROM get_user_scores({argument_list})"
    rows = await conn.fetch(str(query), *args)
    return rows


def _add_arg(arguments, x):
    arguments.append(x)
    return Parameter(f'${len(arguments)}')


class UserIdTempTable(object):

    def __init__(self, conn, users):
        self.conn = conn
        self.users = users

    async def __aenter__(self):
        await self.conn.execute("CREATE TEMP TABLE user_id_temp(id BIGINT NOT NULL);")
        await self.conn.executemany(
            "INSERT INTO user_id_temp(id) VALUES($1)",
            [(user.id,) for user in self.users])

    async def __aexit__(self, exc_type, value, traceback):
        await self.conn.execute("DROP TABLE IF EXISTS user_id_temp;")

