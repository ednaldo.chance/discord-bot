import asyncio
import re

import discord
from discord.ext import commands
from discord.ext.commands.view import StringView


async def do_create_ranking_prompt(ctx, session, prompt):
    prompt_message = "`[1/2]` What is the name of this ranking?"
    await prompt.update(ctx, content=prompt_message)

    def check(message):
        return (message.author == ctx.message.author and
                not message.content.startswith(prompt.bot.command_prefix))

    while True:
        message = await prompt.wait_for_answer(check=check, timeout=60)
        if message is None:
            await prompt.cancel()
            return None

        ranking_name = message.content
        await message.delete()

        if len(ranking_name) > 32:
            warning = "Ranking name must be at most 32 characters long. Please try again."
            text = format_ranking_name_message(prompt_message, warning)
            await prompt.update(ctx, content=text)
            continue

        is_alphanum = bool(re.match('^[a-zA-Z0-9]+$', ranking_name))
        if not is_alphanum:
            warning = "Ranking name must consist of only alphanumeric characters. Please try again."
            text = format_ranking_name_message(prompt_message, warning)
            await prompt.update(ctx, content=text)
            continue

        guild = ctx.message.guild
        ranking = await session.get_ranking_by_name_and_guild(ranking_name, guild)
        if ranking:
            warning = f"Ranking **{ranking_name}** already exists. Please try again."
            text = format_ranking_name_message(prompt_message, warning)
            await prompt.update(ctx, content=text)
            continue

        break

    text = format_ranking_roles_message(included=[], excluded=[])
    await prompt.update(ctx, content=text)

    command = None
    args = []
    def check(message):
        nonlocal command
        nonlocal args
        if message.content.startswith(prompt.bot.command_prefix):
            return False
        command, args = get_command_and_args(message.content)
        return command in ['include', 'exclude', 'done']

    included_roles = []
    excluded_roles = []

    while True:
        message = await prompt.wait_for_answer(check=check, timeout=60)
        if message is None:
            await prompt.cancel()
            return None

        if command == 'done':
            if not included_roles and not excluded_roles:
                text = format_ranking_roles_message(included=included_roles, excluded=excluded_roles, warning=True)
                await prompt.update(ctx, content=text)
                continue
            else:
                break
        elif command == 'include':
            for arg in args:
                try:
                    role = await commands.RoleConverter().convert(ctx, arg)
                except commands.errors.BadArgument:
                    pass
                else:
                    if role not in included_roles:
                        included_roles.append(role)
                    try:
                        excluded_roles.remove(role)
                    except ValueError:
                        pass
        elif command == 'exclude':
            for arg in args:
                try:
                    role = await commands.RoleConverter().convert(ctx, arg)
                except commands.errors.BadArgument:
                    pass
                else:
                    if role not in excluded_roles:
                        excluded_roles.append(role)
                    try:
                        included_roles.remove(role)
                    except ValueError:
                        pass

        await message.delete()
        text = format_ranking_roles_message(included=included_roles, excluded=excluded_roles)
        await prompt.update(ctx, content=text)

    await message.delete()
    await prompt.cancel()

    return ranking_name, included_roles, excluded_roles


async def do_create_rolegroup_prompt(ctx, session, prompt):
    prompt_message = "`[1/2]` What is the name of this rolegroup?"

    await prompt.update(ctx, content=prompt_message)

    def check(message):
        return (message.author == ctx.message.author and
                not message.content.startswith(prompt.bot.command_prefix))

    while True:
        message = await prompt.wait_for_answer(check=check, timeout=60)
        if message is None:
            await prompt.cancel()
            return None

        rolegroup_name = message.content
        await message.delete()

        if len(rolegroup_name) > 32:
            warning = "Ranking name must be at most 32 characters long. Please try again."
            text = format_rolegroup_name_message(prompt_message, warning)
            await prompt.update(ctx, content=text)
            continue

        is_alphanum = bool(re.match('^[a-zA-Z0-9]+$', rolegroup_name))
        if not is_alphanum:
            warning = "Ranking name must consist of only alphanumeric characters. Please try again."
            text = format_rolegroup_name_message(prompt_message, warning)
            await prompt.update(ctx, content=text)
            continue

        guild = ctx.message.guild
        rolegroup = await session.get_rolegroup_by_name_and_guild(rolegroup_name, guild)
        if rolegroup:
            warning = f"Ranking **{rolegroup_name}** already exists. Please try again."
            text = format_rolegroup_name_message(prompt_message, warning)
            await prompt.update(ctx, content=text)
            continue
        break

    text = format_rolegroup_roles_message(roles=[])
    await prompt.update(ctx, content=text)

    command = None
    args = []
    def check(message):
        nonlocal command
        nonlocal args
        if message.content.startswith(prompt.bot.command_prefix):
            return False
        command, args = get_command_and_args(message.content)
        return command in ['add', 'done']

    roles = []
    while True:
        message = await prompt.wait_for_answer(check=check, timeout=60)
        if message is None:
            await prompt.cancel()
            return None

        if command == 'done':
            if len(roles) < 2:
                text = format_rolegroup_roles_message(roles, warning="You need to add at least two roles.")
                await prompt.update(ctx, content=text)
                continue
            if await session.get_rolegroup_from_roles(*roles):
                text = format_rolegroup_roles_message(roles, warning="A rolegroup with these roles already exists.")
                await prompt.update(ctx, content=text)
                continue
            break
        elif command == 'add':
            for arg in args:
                try:
                    role = await commands.RoleConverter().convert(ctx, arg)
                except commands.errors.BadArgument:
                    pass
                else:
                    if role not in roles:
                        roles.append(role)

        await message.delete()
        text = format_rolegroup_roles_message(roles=roles)
        await prompt.update(ctx, content=text)

    await message.delete()
    await prompt.cancel()

    return rolegroup_name, roles


def format_ranking_name_message(prompt, warning):
    return "{prompt}\n\n:exclamation: {warning}".format(prompt=prompt, warning=warning)


def format_ranking_roles_message(included, excluded, warning=False):
    prompt = (
        "`[2/2]` Use the following commands to include or exclude roles from the ranking:\n\n"
        "`include [roles]`\n"
        "`exclude [roles]`\n\n"
        "When you're done, say `done`.\n"
    )

    block = (
        "```py\n"
        "Include\n\n"
        "{include}\n\n"
        "Exclude\n\n"
        "{exclude}\n\n"
        "```"
    )

    if included:
        inc = '\n'.join([f"  {role.name}" for role in included])
    else:
        inc = "  # None"

    if excluded:
        exc = '\n'.join([f"  {role.name}" for role in excluded])
    else:
        exc = "  # None"

    text = "{prompt}{block}{warning}".format(
        prompt=prompt,
        block=block.format(include=inc, exclude=exc),
        warning="\n:exclamation: You need to include or exclude at least one role.\n" if warning else "",
    )

    return text


def format_rolegroup_name_message(prompt, warning):
    return "{prompt}\n\n:exclamation: {warning}".format(prompt=prompt, warning=warning)


def format_rolegroup_roles_message(roles, warning=None):
    prompt = "`[2/2]` Use `add [roles]` to add roles to the ranking.\n\nWhen you're done, say `done`.\n"
    block = "```\n{}\n```".format('\n'.join([role.name for role in roles]))

    text = "{prompt}{block}\n".format(prompt=prompt, block=block)
    if warning is not None:
        text += f":exclamation: {warning}"

    return text


def get_command_and_args(text):
    view = StringView(text)
    command = view.get_word()
    args = []
    while not view.eof:
        view.skip_ws()
        args.append(view.get_quoted_word())
    return command, args

