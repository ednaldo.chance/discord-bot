class BaseError(Exception):
    pass


class RejoinRoleError(BaseError):

    def __init__(self, role):
        super().__init__(f"You already joined **{role}**.")


class LeaveUnjoinedRoleError(BaseError):

    def __init__(self, role):
        super().__init__(f"You have not joined **{role}**.")


class RolegroupAlreadyExists(BaseError):

    def __init__(self, argument):
        super().__init__(f"Rolegroup named **{argument}** already exists.")


class RolegroupWithRolesAlreadyExists(BaseError):

    def __init__(self):
        super().__init__("A rolegroup with the roles given already exists.")


class RankingAlreadyExists(BaseError):

    def __init__(self, argument):
        super().__init__(f"Ranking **{argument}** already exists.")


class CannotDeleteDefaultRanking(BaseError):

    def __init__(self):
        super().__init__("Cannot delete default ranking.")


class NotEnoughArguments(BaseError):
    pass

