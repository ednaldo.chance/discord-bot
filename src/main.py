import os.path as path
import asyncio
import logging

import asyncpg

import g
import secret
from bot import Bot


async def init_db():
    conn = await asyncpg.connect(
        user=secret.PG_USER, password=secret.PG_PASS,
        host='localhost', database='postgres')
    db_exists = await database_exists(conn, g.dbname)
    if not db_exists:
        await create_database(conn, g.dbname)
    await conn.close()

    if not db_exists:
        conn = await asyncpg.connect(
            user=secret.PG_USER, password=secret.PG_PASS,
            host='localhost', database=g.dbname)
        schema_path = path.join(g.root, 'schema.sql')
        with open(schema_path, 'r') as schema_file:
            await conn.execute(schema_file.read())
        await conn.close()


async def database_exists(conn, name):
    query = "SELECT datname FROM pg_catalog.pg_database WHERE datname = $1"
    row = await conn.fetchrow(query, name)
    return row is not None


async def create_database(conn, name):
    query = f"CREATE DATABASE {name} OWNER postgres"
    await conn.execute(query)


def main():
    log_path = path.join(g.root, 'bot.log')
    handlers = [logging.FileHandler(log_path , 'a', 'utf-8')]
    level = logging.INFO
    fmt = '(%(name)s) %(levelname)s: %(message)s'
    logging.basicConfig(handlers=handlers, level=level, format=fmt)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(init_db())

    bot = Bot()
    bot.run(secret.TOKEN)

