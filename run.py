import os
import sys

root = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(root, 'src'))

import g
g.root = root

from main import main
main()

